/*
  This file is part of @camper-fcc's Random Quote Machine.

  @camper-fcc's Random Quote Machine is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Random Quote Machine is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Random Quote Machine.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React from "react";
import styled from "styled-components";

const BlockQuote = styled.blockquote`
  line-height: 1.5;
  font-style: italic;
  font-size: ${props => props.theme.primaryTextSize};
  transition: display 2.5s linear;

  @media only screen and (min-width: ${props =>
    props.theme.minWidthScreenSize1}) {
    font-size: ${props => props.theme.mediaTextSize1};
  }

  @media only screen and (min-width: ${props =>
    props.theme.minWidthScreenSize2}) {
    font-size: ${props => props.theme.mediaTextSize2};
  }

  @media only screen and (min-width: ${props =>
    props.theme.minWidthScreenSize3}) {
    font-size: ${props => props.theme.mediaTextSize3};
`;

export default ({ text }) => <BlockQuote id="text">{text}</BlockQuote>;
