/*
  This file is part of @camper-fcc's Random Quote Machine.

  @camper-fcc's Random Quote Machine is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Random Quote Machine is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Random Quote Machine.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React from "react";
import Quote from "./Quote";
import Author from "./Author";
import styled, { keyframes } from "styled-components";

const fadein = keyframes`
  0% { opacity: 0; }
  100% { opacity: 1; }
`;

const Main = styled.main`
  padding: 110px 0 80px 0;
  display: flex;
  align-items: center;
  animation: ${fadein} 3s linear;
`;

const StyledDiv = styled.div`
  width: 100%;
`;

export default ({ text, author, link }) => (
  <Main>
    <StyledDiv>
      <Author author={author} link={link} />
      <Quote text={text} />
    </StyledDiv>
  </Main>
);
