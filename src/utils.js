/*
  Utility functions

  This file is part of @camper-fcc's Random Quote Machine.

  @camper-fcc's Random Quote Machine is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Random Quote Machine is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Random Quote Machine.  If not,
  see <https://www.gnu.org/licenses/>.
*/

// Convet HTML entities to symbols, or strip them from text.
export const convertHTML = str => {
  const conversions = {
    "&mdash;": "—",
    "&hellip;": "...",
    "&#038;": "&",
    "&#123;": "{",
    "&#124;": "|",
    "&#125;": "}",
    "&#126;": "~",
    "&#167;": "§",
    "&#169;": "©",
    "&#174;": "®",
    "&#224;": "à",
    "&#225;": "á",
    "&#226;": "â",
    "&#227;": "ã",
    "&#228;": "ä",
    "&#229;": "å",
    "&#230;": "æ",
    "&#231;": "ç",
    "&#232;": "è",
    "&#233;": "é",
    "&#234;": "ê",
    "&#235;": "ë",
    "&#236;": "ì",
    "&#237;": "í",
    "&#238;": "î",
    "&#239;": "ï",
    "&#8211;": "—",
    "&#8212;": "—",
    "&#8216;": "'",
    "&#8217;": "'",
    "&#8220;": '"',
    "&#8221;": '"',
    "&#8230;": "...",
    "&#8243;": '"'
  };

  const regex = /&#233;|&#8243;|&#038;|&#8211;|&#8212;|&mdash;|&#8230|&#8216;|&#8217;|&#8220;|&#8221;|&hellip;|&.+;|<\w+>|<\/\w+>|<\w+\/>|<\w+ \/>/g;
  return str.replace(regex, find => conversions[find] || "");
};

// Generata a random number
export const getRandomNumber = () => Math.floor(Math.random() * 40);

// Creates random hex for random color
export const randomHex = () => {
  let letters = "0123456789ABCDEF";
  let color = "#";
  for (let i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
};

export default { convertHTML, getRandomNumber, randomHex };
