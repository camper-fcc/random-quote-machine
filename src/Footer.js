/*
  This file is part of @camper-fcc's Random Quote Machine.

  @camper-fcc's Random Quote Machine is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Random Quote Machine is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Random Quote Machine.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React from "react";
import styled from "styled-components";
import Button from "./Button";
import Social from "./Social";

const Footer = styled.footer`
  display: flex;
  justify-content: space-between;
  position: fixed;
  bottom: 0;
  background-color: ${props => props.theme.primaryBackgroundColor};
  width: 100%;
  max-width: ${props => props.theme.maxWidth};
  padding: 10px 0;
  border-top: ${props => props.theme.border};
  transition: border 2.5s linear;
`;

export default ({ onClick, text, author }) => (
  <Footer>
    <Button onClick={onClick} />
    <Social text={text} author={author} />
  </Footer>
);
