# Random Quote Generator

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [freeCodeCamp challenge](#freecodecamp-challenge)
- [Live Demo](#live-demo)
- [General Features](#general-features)
- [React Features](#react-features)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## freeCodeCamp challenge
https://learn.freecodecamp.org/front-end-libraries/front-end-libraries-projects/build-a-random-quote-machine

## Live Demo
https://camper-fcc.gitlab.io/random-quote-machine

## General Features

- Hosted on [GitLab Pages](https://about.gitlab.com/features/pages/)
- Uses [semantic HTML5](https://internetingishard.com/html-and-css/semantic-html/) - [fCC Guide on Semantic HTML](https://guide.freecodecamp.org/html/html5-semantic-elements)
- Uses [https://quotesondesign.com](https://quotesondesign.com) for quotes API
- [GNU AGPLv3](https://www.gnu.org/licenses/agpl-3.0.en.html) Licensed
- [DocToc](https://github.com/thlorenz/doctoc) to build README Table of Contents

## React Features

- Built with Create React App
- Each component is responsible for rendering one thing
- Uses [Styled Components](https://www.styled-components.com/) for CSS
- Sanitize HTML, random hex color, and random number functions in custom utils.js

