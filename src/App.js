/*
  This file is part of @camper-fcc's Random Quote Machine.

  @camper-fcc's Random Quote Machine is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Random Quote Machine is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Random Quote Machine.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React, { Component } from "react";
import { ThemeProvider, createGlobalStyle } from "styled-components";
import Header from "./Header";
import Main from "./Main";
import Footer from "./Footer";
import { convertHTML, getRandomNumber, randomHex } from "./utils";

const theme = {
  primaryBackgroundColor: "rgb(255, 254, 255)",
  primaryTextColor: "rgba(0, 0, 0, 0.75)",
  secondaryTextColor: "rgba(255, 254, 255, 1)",
  primaryLinkColor: "rgba(173, 26, 0, 1)",
  primaryTextSize: "7vmin",
  border: "5px solid rgba(173, 26, 0, 1)",
  mediaTextSize1: "6vmin",
  mediaTextSize2: "4vmin",
  mediaTextSize3: "5vmin",
  minWidthScreenSize1: "650px",
  minWidthScreenSize2: "881px",
  minWidthScreenSize3: "2500px",
  maxWidth: "1200px"
};

const GlobalStyle = createGlobalStyle`
  body {
    font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen",
    "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue",
    sans-serif;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    width: 100%;
    max-width: ${theme.maxWidth};
    margin: auto;
    color: ${theme.primaryTextColor};
    text-align: center;
  }

  .root {
    height: 100vh;
    background-color: ${theme.primaryBackgroundColor};
  }

  p {
    margin: 10px 0;
  }

  a {
    color: ${theme.primaryLinkColor};
  }
`;

class App extends Component {
  constructor(props) {
    super(props);

    const randomNumber = getRandomNumber();

    this.state = {
      borderColor: randomHex(),
      quoteData: [],
      quoteText: "",
      quoteAuthor: ""
    };

    fetch(
      "https://quotesondesign.com/wp-json/posts?filter[orderby]=rand&filter[posts_per_page]=40"
    )
      .then(response => response.json())
      .then(data =>
        this.setState({
          quoteData: data,
          quoteText: data[randomNumber].content,
          quoteAuthor: data[randomNumber].title,
          quoteLink: data[randomNumber].link
        })
      );
  }

  componentDidUpdate() {
    document.querySelector("header").style.borderColor = this.state.borderColor;
    document.querySelector("footer").style.borderColor = this.state.borderColor;
  }

  handleClick = e => {
    e.preventDefault();

    const randomNumber = getRandomNumber();

    this.setState({
      borderColor: randomHex(),
      quoteText: this.state.quoteData[randomNumber].content,
      quoteAuthor: this.state.quoteData[randomNumber].title,
      quoteLink: this.state.quoteData[randomNumber].link
    });
  };

  render() {
    return (
      <ThemeProvider theme={theme}>
        <div id="quote-box">
          <GlobalStyle />

          <Header />

          <Main
            text={convertHTML(this.state.quoteText)}
            author={convertHTML(this.state.quoteAuthor)}
            link={this.state.quoteLink}
          />

          <Footer
            onClick={this.handleClick}
            text={convertHTML(this.state.quoteText)}
            author={convertHTML(this.state.quoteAuthor)}
          />
        </div>
      </ThemeProvider>
    );
  }
}

export default App;
