/*
  This file is part of @camper-fcc's Random Quote Machine.

  @camper-fcc's Random Quote Machine is free software: you can redistribute
  it and/or modify it under the terms of the GNU Affero General Public License
  as published by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  @camper-fcc's Random Quote Machine is distributed in the hope that it will
  be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.

  You should have received a copy of the GNU Affero General Public License
  along with @camper-fcc's Random Quote Machine.  If not,
  see <https://www.gnu.org/licenses/>.
*/

import React from "react";
import styled from "styled-components";

const Button = styled.button`
  border: 0;
  padding: 10px;
  border-radius: 5px;
  margin-left: 10px;
  background-color: ${props => props.theme.primaryLinkColor};
  color: ${props => props.theme.secondaryTextColor};
  font-size: 1.3rem;

  :hover {
    cursor: pointer;
  }
`;

export default ({ onClick }) => (
  <Button id="new-quote" className="new-quote-button" onClick={onClick}>
    New Quote
  </Button>
);
